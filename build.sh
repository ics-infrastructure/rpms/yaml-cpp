#!/bin/bash
set -e

# Download sources
mkdir -p SOURCES
spectool -g -C ./SOURCES SPECS/yaml-cpp.spec
# Build RPM
rpmbuild -v --define "%_topdir `pwd`" -bb SPECS/yaml-cpp.spec
