# This file is based on the EPEL8 spec file provided by the community at
# https://src.fedoraproject.org/rpms/yaml-cpp/tree/epel8

%global sover 0.6

Name:           yaml-cpp
Version:        0.6.2
Release:	0
Summary:        A YAML parser and emitter for C++
License:        MIT 
URL:            https://github.com/jbeder/yaml-cpp
Source0:        https://github.com/jbeder/yaml-cpp/archive/%{name}-%{version}.tar.gz

Patch0:         yaml-cpp-static.patch
Patch1:         yaml-cpp-include_dir.patch
Patch2:         CVE-2017-5950.patch

BuildRequires: make
BuildRequires: cmake gcc gcc-c++

%description
yaml-cpp is a YAML parser and emitter in C++ written around the YAML 1.2 spec.


%package        devel
Summary:        Development files for %{name}
License:        MIT
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

	
%package        static
Summary:        Static library for %{name}
License:        MIT
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}
	
%description    static
The %{name}-static package contains the static library for %{name}.
	

%prep
%autosetup -p1 -n %{name}-%{name}-%{version}

%build
rm -rf build_shared && mkdir build_shared
rm -rf build_static && mkdir build_static

pushd build_shared
%cmake -DYAML_CPP_BUILD_TOOLS=OFF \
       -DBUILD_SHARED_LIBS=ON \
       -DYAML_CPP_BUILD_TESTS=OFF \
       ../
%make_build 
popd
pushd build_static
%cmake -DYAML_CPP_BUILD_TOOLS=OFF \
       -DBUILD_SHARED_LIBS=OFF \
       -DYAML_CPP_BUILD_TESTS=OFF \
       -DCMAKE_CXX_FLAGS="-fPIC" \
       ../
%make_build
popd


%install
pushd build_shared
%make_install
popd
pushd build_static
%make_install yaml-cpp
popd

%ldconfig_scriptlets


%files
%doc CONTRIBUTING.md README.md
%license LICENSE
%{_libdir}/*.so.%{sover}*

%files devel
%{_includedir}/yaml-cpp/
%{_libdir}/*.so
%{_libdir}/cmake/%{name}
%{_libdir}/pkgconfig/%{name}.pc

%files static
%license LICENSE
%{_libdir}/*.a
%{_libdir}/cmake/%{name}-static
%{_libdir}/pkgconfig/%{name}-static.pc
